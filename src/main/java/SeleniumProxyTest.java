import io.github.bonigarcia.wdm.WebDriverManager;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponse;
import net.lightbody.bmp.BrowserMobProxy;
import net.lightbody.bmp.BrowserMobProxyServer;
import net.lightbody.bmp.client.ClientUtil;
import net.lightbody.bmp.proxy.CaptureType;
import org.littleshoot.proxy.HttpFilters;
import org.littleshoot.proxy.HttpFiltersAdapter;
import org.littleshoot.proxy.HttpFiltersSourceAdapter;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.netty.handler.codec.http.HttpHeaders;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class SeleniumProxyTest {

    public static void main(String[] args) throws Exception {

        WebDriverManager.chromedriver().setup();
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        BrowserMobProxy proxy = new BrowserMobProxyServer();
        proxy.setTrustAllServers(true);
        Set<CaptureType> captureTypes = new HashSet<>();
        captureTypes.addAll(CaptureType.getAllContentCaptureTypes());
        captureTypes.addAll(CaptureType.getHeaderCaptureTypes());
        captureTypes.addAll(CaptureType.getCookieCaptureTypes());
        proxy.setHarCaptureTypes(captureTypes);
        proxy.enableHarCaptureTypes(captureTypes);
        proxy.addLastHttpFilterFactory(new HttpFiltersSourceAdapter() {
            @Override
            public HttpFilters filterRequest(HttpRequest originalRequest) {
                return new HttpFiltersAdapter(originalRequest) {
                    @Override
                    public HttpResponse proxyToServerRequest(HttpObject httpObject) {
                        if (httpObject instanceof HttpRequest) {
                            ((HttpRequest) httpObject).headers().remove(HttpHeaders.Names.VIA);
                        }
                        return null;
                    }

                    @Override
                    public HttpObject proxyToClientResponse(HttpObject httpObject) {
                        if (httpObject instanceof HttpRequest) {
                            ((HttpResponse) httpObject).headers().remove(HttpHeaders.Names.VIA);
                        }
                        return httpObject;
                    }
                };
            }
        });
        proxy.start(8080);
        proxy.newHar();

        Proxy seleniumProxy = ClientUtil.createSeleniumProxy(proxy);
        capabilities.setCapability(CapabilityType.PROXY, seleniumProxy);

        ChromeOptions options = new ChromeOptions();
        options.addArguments("headless");
        options.addArguments("--no-sandbox");
        options.addArguments("--disable-gpu");
        options.addArguments("--incognito");

        ChromeDriver driver = new ChromeDriver(options.merge(capabilities));
        driver.get("http://example.com");

        driver.quit();

        proxy.getHar().writeTo(new File("frontend-performance.har"));
        proxy.stop();

    }
}
